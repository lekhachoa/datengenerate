﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.IO;

namespace DatenbankGenerate
{
    public class RandomContainer<T> : List<T>
    {
        public T PickOne()
        {
            Random random = new Random();
            return this[random.Next(this.Count())];
        }
    };
    public class CPU
    {
        public string Marke { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public byte Core { get; set; }
        public float Takt { get; set; }
        public double TurboBoot { get; set; }
        public byte Cache { get; set; }

        private readonly RandomContainer<string> prozessorMarkeAry = new RandomContainer<string> { "Intel", "AMD" };
        private readonly RandomContainer<string> prozessorNameIntelAry = new RandomContainer<string> { "Intel Celeron", "Intel Xeon", "Intel Core i3", "Intel Core i5", "Intel Core i7" };
        private readonly RandomContainer<string> prozessorNameAMDAry = new RandomContainer<string> { "AMD Ryzen 3", "AMD Ryzen 5", "AMD Ryzen 7", "AMD Threadripper" };
        private readonly RandomContainer<string> prozessorCodeIntelAry = new RandomContainer<string> { "8250U", "8265U", "6700T", "8259U", "8121U", "Z3700", "8565U", "10710U", "1065G7" };
        private readonly RandomContainer<string> prozessorCodeAMDAry = new RandomContainer<string> { "2600U", "3500U", "3700U", "2200U", "3200U", "2200U", "3700X", "3400G" };
        private readonly RandomContainer<byte> prozessorCoreAry = new RandomContainer<byte> { 2, 4, 6, 8 };
        private readonly RandomContainer<float> prozessorTaktAry = new RandomContainer<float> { 2.0f, 2.1f, 2.2f, 2.3f, 2.4f, 2.5f, 2.6f, 2.7f, 2.8f, 2.9f, 3.0f, 3.1f, 3.2f, 3.3f, 3.4f, 3.5f, 3.6f, 3.7f, 3.8f };
        private readonly RandomContainer<byte> prozessorCacheAry = new RandomContainer<byte> { 4, 6, 8 };

        public CPU()
        {
            Marke = prozessorMarkeAry.PickOne();
            Name = Marke == "Intel" ? prozessorNameIntelAry.PickOne() : prozessorNameAMDAry.PickOne();
            Code = Marke == "Intel" ? prozessorCodeIntelAry.PickOne() : prozessorCodeAMDAry.PickOne();
            Core = prozessorCoreAry.PickOne();
            Takt = prozessorTaktAry.PickOne();
            TurboBoot = Math.Round(Takt + 1.2f, 1);
            Cache = prozessorCacheAry.PickOne();
        }
    }
    public class Bildschirm
    {
        public int DisplayGröße { get; set; }
        public string Auflösung { get; set; }
        public string DisplayArt { get; set; }
        public int Leuchtdichte { get; set; }

        private readonly string[] resolutionAry = new string[] { "1920x1080(FullHD)", "1280x720p(HD)", "2048x1080(2K)", "3840×2160(4K)" };
        private readonly  RandomContainer<string> displayArtAry = new RandomContainer<string> { "mattes Display", "IPS-Panel", "glänzendes Display", "Touchscreen" };
        private readonly  RandomContainer<int> brightnessAry = new RandomContainer<int> { 200, 250, 280, 300, 320, 350, 400, 450, 500 };
        public Bildschirm()
        {
            Random random = new Random();
            DisplayGröße = random.Next(11, 18);
            Auflösung = resolutionAry[random.Next(resolutionAry.Length)];
            DisplayArt = displayArtAry.PickOne();
            Leuchtdichte = brightnessAry.PickOne();
        }
    }
    public class Graphic
    {
        public string Hersteller;
        public string Grafikkarte;

        private readonly string[] randHsl = new string[] { "Intel", "Nvidia", "AMD" };
        private readonly RandomContainer<string> IntelGraphics = new RandomContainer<string> { "HD620", "UHD620", "UHD640", "Iris Pro", "Iris Plus" };
        private readonly RandomContainer<string> AMDGraphics = new RandomContainer<string> { "Radeon Vega 8", "Radeon Vega 10", "Radeon RX Vega 10" };
        private readonly RandomContainer<string> NvidiaGraphics = new RandomContainer<string> { "Geforce GTX 1050", "Geforce GTX 1060", "Geforce GTX 1065", "Geforce RTX 2060", "Geforce MX150", "Geforce MX230", "Geforce MX250" };

        public Graphic(string cpuName)
        {
            Random rand = new Random();
            Hersteller = cpuName == "Intel" ? randHsl[rand.Next(2)] : randHsl[rand.Next(1, 3)];
            Grafikkarte = Hersteller == "Intel" ? IntelGraphics.PickOne() : Hersteller == "AMD" ? AMDGraphics.PickOne() : NvidiaGraphics.PickOne();
        }
    }
    public class HDD
    {
        public string Typ { get; set; }
        public string Format { get; set; }
        public int Kapazität { get; set; }

        private readonly RandomContainer<string> hddFormatAry = new RandomContainer<string> { "M.2", "Sata" };
        private readonly RandomContainer<int> hddCapacity = new RandomContainer<int> { 256, 500, 1000, 2000 };

        public HDD()
        {
            Random ran = new Random();
            Typ = ran.Next(2) == 0 ? "SSD" : "HDD";
            Format = Typ == "HDD" ? "Sata" : hddFormatAry.PickOne();
            Kapazität = hddCapacity.PickOne();
        }
    }

    public class Laptop
    {
        public string Artikelnummer { get; set; }
        public string Name { get; set; }
        public string Marke { get; set; }
        public string Series { get; set; }
        public CPU Prozessor { get; set; }
        public Bildschirm Display { get; set; }
        public Graphic Grafik { get; set; }
        public int Arbeitsspeicher { get; set; }
        public HDD Festplatte { get; set; }
        public bool HDMI { get; set; }
        public float USB { get; set; }
        public bool CardReader { get; set; }
        public float Bluetooth { get; set; }
        public bool WLan { get; set; }
        public string Farbe { get; set; }
        public float Gewicht { get; set; }
        public int Akku { get; set; }
        public string Betriebssystem { get; set; }
        public int Preis { get; set; }


        public Laptop()
        {
            Random random = new Random();
            Prozessor = new CPU();
            Grafik = new Graphic(Prozessor.Marke);
            Display = new Bildschirm();
            Festplatte = new HDD();
            HDMI = random.NextDouble() > 0.5;
            Gewicht = (float)random.Next(9, 20) / 10;
            CardReader = random.Next(2) == 0;
            WLan = random.Next(2) == 0;
            Akku = random.Next(32, 51);
            Artikelnummer = $"A{random.Next(500000, 900000)}";

            Arbeitsspeicher = new RandomContainer<byte> { 4, 8, 12, 16, 32 }.PickOne();
            USB = new RandomContainer<float> { 2.0f, 3.0f, 3.1f, 3.2f }.PickOne();
            Farbe = new RandomContainer<string> { "Silver", "White", "Blue", "Black", "Yellow", "Red", "Cyan", "Violet", "Indigo", "Pink" }.PickOne();
            Bluetooth = new RandomContainer<float> { 4.0f, 4.1f, 4.2f, 5.0f }.PickOne();
            Betriebssystem = new RandomContainer<string> { "Windows 10", "Windows 7", "Linux" }.PickOne();
            Marke = new RandomContainer<string> { "Samsung", "LG", "Asus", "Dell", "Acer", "Lenovo", "HP" }.PickOne();
            Series = $"{Marke.Substring(0, 2).ToUpper()}{Display.DisplayGröße}";
            Name = $"{Marke} {Series} {Prozessor.Name} {Prozessor.Code} / {Display.DisplayGröße}in / {Grafik.Hersteller} {Grafik.Grafikkarte} / {Arbeitsspeicher}GB Ram / {Festplatte.Typ} {Festplatte.Kapazität}GB / {Betriebssystem}";
            Preis = ((random.Next(15, 30) * 5)-1)*10;
        }
    }

    public static class Program
    {
        static void Main(string[] args)
        {
            int NumberOfLaptop = 100;

            Console.WriteLine($"Creating Database with {NumberOfLaptop} Elements for running and testing Project");
            List<Laptop> LaptopDB = new List<Laptop>();
            for (int i = 0; i < NumberOfLaptop; i++)
                LaptopDB.Add(new Laptop());

            //Console.WriteLine(String.Join("\n", LaptopDB));
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string filePath = desktopPath + @"\LaptopDb.json";

            var serializer = new JsonSerializer { Formatting = Formatting.Indented, TypeNameHandling = TypeNameHandling.Auto };
            //var serializer = new JsonSerializer { TypeNameHandling = TypeNameHandling.Auto };
            using (StreamWriter writer = File.CreateText(filePath)) { serializer.Serialize(writer, LaptopDB); }
            
            Console.WriteLine("Database was finised creating on Desktop with name \"LaptopDb.json\"");
            Console.WriteLine("Pls use an online json viewer eg: jsoneditoronline.org for a clear view");
            //Mình thích thì mình add
            Console.ReadKey();
        }
    }
}
